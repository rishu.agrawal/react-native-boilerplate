import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';

import HomeNavigator from './navigators/home';

export default function AppNavigation() {
  return (
    <NavigationContainer>
      <HomeNavigator />
    </NavigationContainer>
  );
}
